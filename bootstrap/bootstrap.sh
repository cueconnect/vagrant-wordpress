#!/usr/bin/env bash

# Define vars

DBUSER='root'
DBPASS='d5ertf5'
DBNAME='wp_woocommerce'

# Install LAMP stack
apt-get update

debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASS"

apt-get -yf install lamp-server^ curl php5-curl php5-json php5-imagick php5-gd git
# apt-get -y autoremove
# apt-get -y autoclean

# --- Configure apache

if ! [ -L /var/www ]; then
	rm -rf /var/www
	ln -fs /vagrant /var/www
fi

# add our virtualhost conf file
cp /vagrant/bootstrap/000-default.conf /etc/apache2/sites-available/

# change user that runs apache from www-data to vagrant
# to eliminate file permission problems
sed -i.bak s/www-data/vagrant/g /etc/apache2/envvars

# enable rewrite apache module
a2enmod rewrite

# restart apache
service apache2 restart

# create working folder if it doesn't exist
#rm -rf /vagrant/logs
#ln -s /var/log/apache2 /vagrant/logs

# --- Import database dump
mysql -u$DBUSER -p$DBPASS -e "CREATE DATABASE IF NOT EXISTS \`$DBNAME\`;"
mysql -u$DBUSER -p$DBPASS $DBNAME < /vagrant/bootstrap/migration.sql

# Get wp-cli
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar /usr/bin/wp

# Update plugins
cd /vagrant/html
sudo su vagrant -c "wp plugin update --all"

service apache2 restart
