# Wordpress vagrant box

### Getting started

- Install [vagrant](https://www.vagrantup.com/downloads.html)
- Install [virtualbox](https://www.virtualbox.org/wiki/Downloads)
- Checkout this repo
```bash
    git clone https://bitbucket.org/cueconnect/vagrant-wordpress
    cd vagrant-wordpress
```

- Start vagrant
```bash
    vagrant up
```

- Wait until setup is finished.


### Environment

Woocommerce is available at http://127.0.0.1:1337

[Mysql](http://127.0.0.1:1337/adminer.php)

* username: root
* password: d5ertf5
* database: wp_woocommerce

[Wordpress Dashboard](http://127.0.0.1:1337/wp-admin)

* username: admin
* password: cueconnect

SSH:

Do ```vagrant ssh``` inside project folder


### Cue Connect install script

```
cd vagrant-wordpress
git submodule init
git submodule update
```


### Folder structure

```
/bootstrap - folder with vagrant install scripts and configs
/html - folder with wordpress, available at /var/www/html inside vagrant
```

### Vagrant commands

- start ```vagrant up```
- stop ```vagrant halt```
- restart ```vagrant reload```
- delete ```vagrant destroy```
- update box image and all stuff ```vagrant reload --provision```

For more options please refer to the [site](https://www.vagrantup.com/docs/)

### Additional tools available

- [Adminer](http://127.0.0.1:1337/adminer.php)
- [Phpinfo](http://127.0.0.1:1337/phpinfo.php)
- [wp](http://wp-cli.org) from ssh
- git from ssh


### Who do I talk to? ###

Questions and suggestions are welcome at aanpilov@cueconnect.com or @alex in slack
